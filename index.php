<?php
include_once __DIR__.'/libs/crest/CRest.php';
include_once __DIR__.'/libs/debugger/Debugger.php';
include_once __DIR__.'/blocks.php';
define ('CLIENT', __DIR__.'/libs/crest/settings.json');
define ('PATH', __DIR__.'/log/log.txt');
define ('HANDLER', 'http://ef0bde10.ngrok.io/projects/tpk_osnova/blocks/'); // путь до блока-обработчика
#=======================================================================================================#
if (isset($_REQUEST['PLACEMENT']) && !file_exists(CLIENT)) {
	### Установка приложения ###
	$install = CRest::installApp();
	header("Location: index.php");
	
} elseif (in_array(key($_REQUEST), array_keys($blocks))) {
	if ($_REQUEST[key($_REQUEST)] == 'Установить') {
		### установка действия ###
		$params = require ('setting.php');
		$result = CRest::call('bizproc.activity.add', $params[key($_REQUEST)]);

	} elseif ($_REQUEST[key($_REQUEST)] == 'Удалить') {
		### удаляем действие ###
		$result = CRest::call('bizproc.activity.delete', array('CODE' => key($_REQUEST)));
	}
	header("Location: index.php");

} else {
	### узнать статус действий ###
	$bizActivityList = CRest::call('bizproc.activity.list', array());
	if (isset($bizActivityList['result'])) {
		foreach ($bizActivityList['result'] as $activ) {
			if (in_array($activ, array_keys($blocks))) $status[$activ] = 'Удалить';
		}
	}
	include_once (__DIR__.'/view/index.php'); //верстка
}