<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', true);
#=============================================================#
### Блок которые позволяет получить данные необходимого поля в счете ###
Debugger::writeToLog($_REQUEST, PATH, 'invoiceField:Получили запрос', LOG);
if(isset($_REQUEST['code']) && $_REQUEST['code'] == 'invoiceField') {
	$invoice = CRest::call('crm.invoice.get', array('id' => $_REQUEST['properties']['id'], 'select' => array($_REQUEST['properties']['field'])));
	Debugger::writeToLog($invoice, PATH, 'invoiceField:Получили счет', LOG);

	### ответ ###
	$params = array(
		'EVENT_TOKEN'   => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('outputString' => $invoice['result'][0][$_REQUEST['properties']['field']])
	);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'invoiceField:Ответ процессу', LOG);
}