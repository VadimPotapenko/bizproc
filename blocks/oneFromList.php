<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', true);
#=============================================================#
### Блок который позволяет получить данные из универсального списка ###
###### !!!!!! НЕ НАСТРОЕНА ВЫБОРКА !!!!!!! ##########
Debugger::writeToLog($_REQUEST, PATH, 'oneFromList:Получили запрос', LOG);
if (isset($_REQUEST['code']) && $_REQUEST['code'] == 'oneFromList') {
	$list = CRest::call('lists.element.get', array(
		'IBLOCK_TYPE_ID' => 'lists', 
		'IBLOCK_CODE'    => $_REQUEST['properties']['id'],
		'FILTER'         => array($_REQUEST['properties']['fieldIdForFilter'] => $_REQUEST['properties']['filter']),
		// 'select'         => array($_REQUEST['properties']['needFieldById']) // !!!!!
	));
	Debugger::writeToLog($list, PATH, 'oneFromList:Получили списки', LOG);

	### ответ ###
	$params = array(
		'EVENT_TOKEN'   => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('output' => array($list['result']))
	);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'oneFromList:Ответ процессу', LOG);
}