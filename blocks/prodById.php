<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', true);
#=============================================================#
### Блок который позволяет получить товары по ID ###
Debugger::writeToLog($_REQUEST, PATH, 'prodById:Получили запрос', LOG);
if (isset($_REQUEST['code']) && $_REQUEST['code'] == 'prodById') {
	$product = CRest::call('crm.product.list', array(
		'filter' => array('ID' => $_REQUEST['properties']['id']),
		'select' => array($_REQUEST['properties']['field'])
	));
	Debugger::writeToLog($product, PATH, 'prodById:Получили данные товара', LOG);

	### ответ ###
	$params = array(
		'EVENT_TOKEN'   => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('output' => array($product['result']))
	);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'prodById:Ответ процессу', LOG);
}