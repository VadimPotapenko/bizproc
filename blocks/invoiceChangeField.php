<?php
#========================= setting ===========================#
include_once (dirname(__DIR__).'/libs/crest/CRest.php');
include_once (dirname(__DIR__).'/libs/debugger/Debugger.php');
define ('PATH', dirname(__DIR__).'/log/blockslog.txt');
define ('LOG', true);
#=============================================================#
### Блок который позволяет изменить значение поля счета ###
if (isset($_REQUEST['code']) && $_REQUEST['code'] == 'invoiceChangeField') {
	Debugger::writeToLog($_REQUEST, PATH, 'invoiceChangeField:Получили запрос');
	$invoice = CRest::call('crm.invoice.update', array(
		'id' => $_REQUEST['properties']['id'],
		'fields' => array($_REQUEST['properties']['field'] => $_REQUEST['properties'['value']])
	));
	$status = ($invoice['result'] == '1') ? 'Y' : 'N';

	### ответ ###
	$params = array(
		'EVENT_TOKEN' => $_REQUEST['event_token'],
		'RETURN_VALUES' => array('outputString' => $status)
	);
	$answer = CRest::call('bizproc.event.send', $params);
	Debugger::writeToLog($answer, PATH, 'invoiceChangeField:Ответ процессу');
}